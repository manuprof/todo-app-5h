console.log('file js caricato');
// dichiaro la funzione async 
// che mi permette di eseguire in modo "sincrono"
// le istruzioni dopo l'awayt

async function addTodo() {
    // qui ci sarà il codice che aggiunge il todo in db via HTTP [POST]
    var inputEl = document.getElementById("addTodoInput");
    var expireDateEl = document.getElementById("expireDateInput");
    // recupero il valore inserito nella casella di testo
    var description = inputEl.value;
    var expireDate = expireDateEl.value;
    await fetch('/api/todos', {
        method: 'POST',
        // aggiungo un header contenente il contentType
        // perché così il server capisce come deserializzare
        headers: {
            'Content-Type': 'application/json'
        },
        // passo un oggetto con la proprietà description che contiene il todo
        // NB l'oggetto da passare deve essere serializzato
        body: JSON.stringify({
            description: description,
            expireDate: expireDate
        })
    });
    // pulisco la casella di testo
    inputEl.value = "";
    // pulisco la data di scadenza
    expireDateEl.value = "";

    // ricarico tutti i todo
    renderTodos();

}
// questa funzione elimina un todo chiamando 
// HTTP [DELETE] passando l'id del todo da eliminare
// api/todos/{id}
async function deleteTodo(id) {
    var response = await fetch('/api/todos/' + id, {
        method: 'DELETE'
    });
    // ricarico tutto
    renderTodos();
}

// questa funzione salva un todo chiamando 
// HTTP [PUT] passando l'id del todo 
// api/todos/{id}
async function saveTodo(id, inputDescription, expireDateEl, checkboxEl) {
    var description = inputDescription.value;
    var expireDate = expireDateEl.innerText;
    var done = checkboxEl.checked;
    var response = await fetch('/api/todos/' + id, {
        method: 'PUT',
        // aggiungo un header contenente il contentType
        // perché così il server capisce come deserializzare
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            description: description,
            done: done ? 1 : 0,
            expireDate: expireDate
        })
    });
    // ricarico tutto
    renderTodos();
}

// ricarica i todos e li stampa 
async function renderTodos() {
    var ulElement = document.getElementById("todosListUl");
    // creiamo una chiamata http verso [GET] /api/todos
    // N.B. await dice al browser di aspettare il risultato della fetch
    var response = await fetch('/api/todos');
    var todos = await response.json();
    // eventualmente svuoto il todo
    // la prima volta non ce ne saranno
    // però successivamente potrebbero essercene
    ulElement.innerHTML = '';

    // faccio un classico for da terzo superiore
    // per stampare i todo nella pagina html
    for (var i = 0; i < todos.length; i++) {
        // pesco il todo-iesimo
        var todo = todos[i];
        var liElement = document.createElement('li');
        var deleteButton = document.createElement('i');
        var saveButton = document.createElement('i');
        var divDescription = document.createElement('div');
        var inputDescription = document.createElement('input');
        var checkboxEl = document.createElement('input'); // todo completo si/no
        checkboxEl.type = "checkbox"; // lo fa diventare un checkbox
        deleteButton.className = "mdi mdi-delete";
        inputDescription.value = todo.description;
        inputDescription.type = "text";
        //inputDescription.style = "border:none;";
        //saveButton.innerText = "Salva";
        saveButton.className = "mdi mdi-content-save";
        divDescription.appendChild(deleteButton);
        divDescription.appendChild(saveButton);
        divDescription.appendChild(checkboxEl); // lo appendo dopo il floppy
        divDescription.appendChild(inputDescription);
        liElement.appendChild(divDescription);
        // quando l'utente clicca il tasto X
        // chiamo la funzione deleteTodo
        var deleteThisTodo = deleteTodo.bind(null, todo.id);
        deleteButton.addEventListener('click', deleteThisTodo);

        var divExpireDate = document.createElement('div');

        // stampo la data solo se presente
        if (todo.expireDate != null) {
            divExpireDate.innerText = todo.expireDate;
            divExpireDate.className = "expireDate";
            //divExpireDate.style = "font-size:11px;color:red;";
        }
        // check l'input type check quando done = 1
        if (todo.done === 1) {
            checkboxEl.checked = true;
        }



        liElement.appendChild(divExpireDate);

        var saveThisTodo = saveTodo.bind(
            null,
            todo.id,
            inputDescription,
            divExpireDate,
            checkboxEl // aggiunto per recuperare la proprietà checked (todo.done)
        );
        saveButton.addEventListener('click', saveThisTodo);

        ulElement.appendChild(liElement);
    }

}

async function main() {
    console.log('documento caricato completamente');
    // prendo il tag div con id=app
    var appDiv = document.getElementById('app');
    // creo il tag input che mi permette di scrivere il nuovo todo
    var inputEl = document.createElement('input');
    // la casella di testo
    inputEl.type = "text";
    inputEl.placeholder = 'Inserisci il testo del todo'; // inserisce una indicazione
    // aggiungo l'id perché mi servirà prenderlo successivamente
    inputEl.id = "addTodoInput";
    appDiv.appendChild(inputEl);
    var buttonEl = document.createElement('button');
    buttonEl.innerText = "Aggiungi";

    // creo una casella di testo per mettere la scadenza
    var expireDateEl = document.createElement('input');
    expireDateEl.id = "expireDateInput"
    expireDateEl.type = 'datetime-local';
    appDiv.appendChild(expireDateEl);
    appDiv.appendChild(buttonEl);

    // attaccio l'evento click sul bottone aggiungi
    buttonEl.addEventListener('click', addTodo);

    // creo il tag ul
    var ulElement = document.createElement('ul');
    ulElement.id = 'todosListUl';
    // aggiungo dentro il tag div app l'elemento ul
    appDiv.appendChild(ulElement);

    // una volta costruita la mia interfaccia
    // carico e renderizzo i todos
    renderTodos();

}


// aspetto che la pagina sia stat caricata completamente
document.addEventListener('DOMContentLoaded', main);